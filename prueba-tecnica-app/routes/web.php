<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MovieController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::any('/home', [HomeController::class,'anyIndex'])->name('load.home'); // tanto por get como por post
Route::any('/home/store_API_info', [MovieController::class,'storeDB'])->name('store.DB'); 

// Cargar la base de datos con un campo de búsqueda mediante Ajax 
Route::any('/home/movies', [MovieController::class,'showMovies'])->name('movies.show');
Route::get('/home/getmovies', [MovieController::class,'getMovies'])->name('movies.get');
