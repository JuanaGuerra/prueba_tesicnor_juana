<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Movie; 
use Illuminate\View\View; 
use Redirect;
//use App\Exceptions\InvalidOrderException;
//use DB; 
use DataTable; 

class MovieController extends Controller
{
    public function storeDB(Request $request){
        $first_m = Movie::first(); 
        if ( !empty($first_m) ){

            return Redirect::back()->withErrors(['msg' => 'Api information already stored in DB']);

        } else {  // obtiene toda la info de Harry Potter de la API y la carga en la BD  

            $m = Http::get("http://www.omdbapi.com/?apikey=731e41f&s=Harry+Potter")->json();
            //dd($m);

            for ($i = 0; $i <= count($m["Search"])-1 ; $i++){
                $new_movie = new Movie();
                $new_movie->imdb_id = $m["Search"][$i]["imdbID"]; 
                $new_movie->titulo = $m["Search"][$i]["Title"]; 
                $new_movie->anio = $m["Search"][$i]["Year"];
                $new_movie->imagen_path = $m["Search"][$i]["Poster"];
                $new_movie->valoracion = rand(1,5); 
                $new_movie->save();
            }
    
            return redirect(route('movies.show'))->with('success','API information stored correctly in DB'); 
        }
        
    }

    public function showMovies(){
        return view('showdatabase');
    }

    public function getMovies(){
        return \DataTables::of(Movie::query())
            ->addColumn('image', function ($movie) {
                return '<img src="' . asset($movie->imagen_path) . '" alt="Imagen">';
            })
            ->rawColumns(['image'])
            ->make(true);
    }

}
