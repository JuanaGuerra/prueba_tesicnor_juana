# APLICACIÓN WEB    

## PREPARACIÓN DEL ENTORNO DE DESARROLLO
* Instalar Xampp
* Instalar Laravel mediante Composer
* Instalar VS Code

## EJECUCIÓN DEL PROYECTO 
Abrir el proyecto con Visual Studio y en una terminal nueva ejecutar el comando php artisan serve para levantar el servidor web de desarrollo de Laravel.  

URL de la página web : http://127.0.0.1:8000/home/
URL de la base de datos: http://localhost/phpmyadmin/

Dentro de la rama principal he añadido la versión final de la aplicación web: 

    * La vista principal consta de dos botones: 

        * Cuando se pulsa el botón Store API information into DB (y todavía no hay información en la base de datos), se realiza una petición a la API sobre la información de las películas de Harry Potter y se almacena en la base de datos. A continuación, se carga una segunda vista donde aparece en una tabla toda la información almacenada en la base de datos. Encima de la tabla existe un campo de filtrado de la información. Conforme se escriba en el campo, se realizará el filtro automáticamente. 

        * El botón Show movies sirve para mostrar en cualquier momento la información almacenada sobre las películas en la base de datos. 
    
En otra rama he subido una segunda versión de la aplicación web: 

    * Esta sigue la misma lógica que la versión anterior, sin embargo, se trata de una primera versión de la aplicación que realicé con una metodología que conocía de antemano. 
