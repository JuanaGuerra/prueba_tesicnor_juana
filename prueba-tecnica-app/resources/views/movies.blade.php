<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Movies</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        
        <style> 

            input[type=text], input[type=number] {
                width: 20%;
                padding: 12px 20px;
                margin: 8px 0;
                box-sizing: border-box;
                border: 1px solid #555;
                border-bottom: 2px solid grey;
            }

            h1, td, thead {
                text-align: center;
            }
   
            button {
                width: 20%;
                padding: 12px 20px;
                margin-right: auto;
            }
        </style>
    </head>
    
    <body class="antialiased" bg-gray-100>
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <div class>

            <form method="post" action="{{ route('load.home') }}">
            @csrf
            @method('post')
                <button type="submit" id="load_home" class="btn btn-link">
                        Back to Home
                </button><br><br>
            </form> 

            <br/><h1> Harry Potter movies </h1><br/>
            <div class="container">
                <form method="GET" id="search-form">
                    <input type="text" name="imbd_id" placeholder="IMDB ID..." value = "{{ request('imbd_id') }}">
                    <input type="text" name="titulo" placeholder="Titulo..." value = "{{ request('titulo') }}">
                    <input type="text" name="anio" placeholder="Año..." value = "{{ request('anio') }}">
                    <input type="number" name="valoracion" min="1" max="5" placeholder="Valoración..." value = "{{ request('valoracion') }}">
                </form>

                <div class>
                    <table id="table_movies" class="table table-bordered data-table">
                        <thead>
                            <tr>    
                                <th> IMBD ID </th>
                                <th> Título </th>
                                <th> Año </th>
                                <th> Imagen </th>
                                <th> Valoración </th>
                            </tr>
                        </thead>
                        <tbody>
                            <@foreach($movies as $movie)
                                <tr>
                                    <td> {{$movie->imdb_id}} </td>
                                    <td> {{$movie->titulo}} </td>
                                    <td> {{$movie->anio}} </td>
                                    <td> <img src="{{ $movie->imagen_path }}" > </td>
                                    <td> {{$movie->valoracion}} </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {!! $movies->appends(request()->query())->links() !!}
            </div>
        </div>   
        
        <script>
            let form = document.querySelector('#search-form'); 

            document.querySelector('input[name="imbd_id"]').addEventListener('keypress', function(e){
                if (e.key === 'Enter'){
                    form.submit(); 
                }
            }); 

            document.querySelector('input[name="titulo"]').addEventListener('keypress', function(e){
                if (e.key === 'Enter'){
                    form.submit(); 
                }
            }); 

            document.querySelector('input[name="anio"]').addEventListener('keypress', function(e){
                if (e.key === 'Enter'){
                    form.submit(); 
                }
            }); 

            document.querySelector('input[name="valoracion"]').addEventListener('keypress', function(e){
                if (e.key === 'Enter'){
                    form.submit(); 
                }
            }); 

        </script>

    </body>

</html>