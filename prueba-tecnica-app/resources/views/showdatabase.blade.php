<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Movies</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/2.0.1/css/dataTables.dataTables.css" />
        <script src="https://cdn.datatables.net/2.0.1/js/dataTables.js"></script>

        <style> 

            h1, th, thead, td {
                text-align: center;
            }
   
            button {
                width: 20%;
                padding: 12px 20px;
                margin-right: auto;
            }

            img {
                width: 80%;
                height: 80%; 
            }

        </style>
    </head>
    
    <body class="antialiased" bg-gray-100>
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <div class>

            <form method="post" action="{{ route('load.home') }}">
            @csrf
            @method('post')
                <button type="submit" id="load_home" class="btn btn-link">
                        Back to Home
                </button><br><br>
            </form> 

            <br/><h1> Harry Potter movies </h1><br/>
            <div class="container">
                <div class>
                    <table id="table_movies" class="table table-bordered data-table">
                        <thead>
                            <tr>    
                                <th> IMBD ID </th>
                                <th> Título </th>
                                <th> Año </th>
                                <th> Imagen </th>
                                <th> Valoración </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>   
        
        <script>
            $(document).ready(function() {
                $('#table_movies').DataTable({
                        "ajax": "{{ route('movies.get') }}",
                        "columns": [
                            {data: 'imdb_id'},
                            {data: 'titulo'},
                            {data: 'anio'},
                            {data: 'image'}, 
                            {data: 'valoracion'}
                        ]
                }); 
            }); 
        </script>

    </body>

</html>

