<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Home page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      
        <style> 

            button {
                width: 20%;
                padding: 12px 20px;
                margin: 8px 100px;
            }

            h1 {
                margin: 10px 100px;
            }

        </style>
     
    </head>
    <body class="antialiased" bg-gray-100>
        <div class>
            
            <h1> Home </h1><br/>

            @if($errors->any())
                <div class="alert alert-warning">
                    {{ $errors->first() }}
                </div>
            @endif
            
            <form method="post" action="{{ route('store.DB') }}">
            @csrf
            @method('post')
                <button type="submit" id="load_info" class="btn btn-primary">
                        Store API information into DB
                </button><br><br>
            </form> 

            <form method="post" action="{{ route('movies.show') }}">
            @csrf
            @method('post')
                <button type="submit" id="show_info" class="btn btn-primary">
                        Show movies
                </button><br><br>
            </form> 

        </div>   
    </body>

</html>

